package kh.com.prasac;

import kh.com.prasac.student.Student;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Student student = new Student();
        student.setId(1);
        student.setName("Dara");
        student.setGender("M");
        student.setEnglishScore(50);
        student.setKhmerScore(101);

        //System.out.println("Khmer Score: " + student.getKhmerScore());
        //System.out.println("English Score: " + student.getEnglishScore());

        System.out.println("Object 1: " + student.toString());


        Student student2 = new Student(2, "Nana", "F",
                99, 89);

        System.out.println("TOTAL: "+ student2.getTotalScore());
        System.out.println("AVG: " + student2.getAvg());

        System.out.println(student2.toString());






    }
}
