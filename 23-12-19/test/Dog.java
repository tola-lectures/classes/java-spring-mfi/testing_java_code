package kh.com.prasac.test;

import java.util.Scanner;

public class Dog {

    String name;
    String breed;
    String size;
    String color;

    String getDogInfo(String name1, String breed, String size, String color){
        this.name = name1;
        this.breed = breed;
        this.size = size;
        this.color = color;
        return this.name + " , " + this.breed + " , "
                + this.size + " , " + this.color;
    }

    void eat(){
        System.out.println("Eat");
    }

    String sleep(){
        System.out.println("Sleep");
        return  "Sleep...";
    }

    String sit(){
        System.out.println("Sit");
        return "Sit !!!";
    }

    void run(){
        System.out.println("Run");
    }

    public static void main(String args[]){
        Dog dog1 = new Dog();
        dog1.eat();

        Dog dog2 = new Dog();
        String getFo = dog2.getDogInfo("name","breed","size","color");
        System.out.println("===>  DogInfo : " + getFo);

        Dog dog3 = new Dog();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter name: ");
        String name = scanner.next();

        System.out.println("Please enter breed: ");
        String breed = scanner.next();

        System.out.println("Please enter size: ");
        String size = scanner.next();

        System.out.println("Please enter color: ");
        String color = scanner.next();

        String getDogInfoFromKeyBoard = dog3.getDogInfo(name , breed, size, color);
        System.out.println(getDogInfoFromKeyBoard);


    }

}

