package kh.com.prasac.student;

public class Student {

    private int id;
    private String name;
    private String gender;
    private double khmerScore;
    private double englishScore;
    private double totalScore;
    private double avg;

    public Student() {
        System.out.println("Default Constructor.");
    }

    public Student(
            int id, String name, String gender,
            double khmerScore, double englishScore
    ){
        //this();
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.khmerScore = khmerScore;
        this.englishScore = englishScore;
    }

    /*public Student(int id, String name, String gender,
                   double khmerScore, double englishScore,
                   double totalScore, double avg){
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.khmerScore = khmerScore;
        this.englishScore = englishScore;
        this.avg = avg;
        this.totalScore = totalScore;
    }*/

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public double getKhmerScore() {
        return khmerScore;
    }

    public void setKhmerScore(double khmerScore) {
        if(khmerScore >= 0 && khmerScore <= 100){
            this.khmerScore = khmerScore;
        }else{
            this.khmerScore = 0;
            System.out.println("Error !!!!!");
        }
    }

    public double getEnglishScore() {
        return englishScore;
    }

    public void setEnglishScore(double englishScore) {
        this.englishScore = englishScore;
    }

    public double getTotalScore() {
        return this.getKhmerScore() + this.getEnglishScore();
    }

    public void setTotalScore(double totalScore) {
        this.totalScore = totalScore;
    }

    public double getAvg() {
        return this.calTotalScore() / 2;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }

    public double calTotalScore(){
        return this.getKhmerScore() + this.getEnglishScore();
    }

    public double calAvg(){
        return this.calTotalScore() / 2;
    }



    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", khmerScore=" + khmerScore +
                ", englishScore=" + englishScore +
                ", totalScore=" + totalScore +
                ", avg=" + avg +
                '}';
    }
}
