package kh.com.prasac;

public class TestStatic {

    int globalVar = staticMethod();; // Global Variable

    static int staticVar = staticMethod(); // static Variable

    // Static block
    static{
        //staticVar = 100;
        System.out.println("From static block");
    }

    static int staticMethod(){
        return 10;
    }

    public void callGlobalVar(){
        System.out.println(globalVar);
    }

    public static void main(String args[]){
        // System.out.println("From Main : " + globalVar); // Error

        System.out.println("From Main : " + staticVar);

        testStatic s = new testStatic();
        s.globalVar = 100;
        s.callGlobalVar();

        testStatic.InnerClass.testInner("Hello");
        testStatic.InnerClass innerClass = new testStatic.InnerClass();
        innerClass.notStaticMethod("notStaticMethod");

        //s.InnerClassNotStaticClass


        new testStatic.InnerClass().notStaticMethod("Test");


    }

    class InnerClassNotStaticClass{

    }

    static class InnerClass{
            public static void testInner(String key){
                System.out.println("From inner class : "+ key);
            }
            public void notStaticMethod(String key){
                System.out.println("notStaticMethod => From inner class : "+ key);
            }

    }

}

class A{

}
