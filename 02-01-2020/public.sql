/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 90602
 Source Host           : localhost
 Source Database       : TEST_DB
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90602
 File Encoding         : utf-8

 Date: 01/03/2020 08:20:53 AM
*/

-- ----------------------------
--  Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS "public"."student";
CREATE TABLE "public"."student" (
	"id" int4,
	"full_name" varchar(100) COLLATE "default",
	"gender" varchar(1) COLLATE "default",
	"major" varchar(100) COLLATE "default",
	"dob" varchar(100) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."student" OWNER TO "postgres";

-- ----------------------------
--  Records of student
-- ----------------------------
BEGIN;
INSERT INTO "public"."student" VALUES ('1', 'SIN VISAL', 'F', 'IT', '1993');
INSERT INTO "public"."student" VALUES ('2', 'Sak Sonita', 'F', 'Khmer Study', '1992');
INSERT INTO "public"."student" VALUES ('3', 'Sak Sonita', 'F', 'Khmer Study', '1992');
COMMIT;

